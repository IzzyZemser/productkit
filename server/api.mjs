// require('dotenv').config();
import express from 'express'
import morgan from 'morgan'
import log from '@ajar/marker'
import cors from 'cors'
import connection from './db/sql.connection.mjs'
import raw from './middleware/route.async.wrapper.mjs';
import user_router from './routes/user.router.mjs';
import auth_router from './routes/auth.router.mjs';
import feature_router from './routes/features.router.mjs';

const { PORT,HOST } = process.env;

const app = express();

// middleware
app.use(cors());
app.use(morgan('dev'))

app.set("trust proxy", 1) // trust first proxy

// routing
app.get( "/",raw(async (req, res) => {
  console.log("here")
  const [rows] =  await connection.query('SELECT * FROM draw.features;')
  res.status(200).json(rows);
}));

app.use('/users', user_router);
app.use('/auth', auth_router)
app.use('/features', feature_router)

//when no routes were matched...
app.use('*', (req,res)=>{
    res.send("NOT FOUND")
})

;(async ()=> {
  //connect to mongo db
  await app.listen(PORT,HOST);
  log.magenta(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
})().catch(console.log)