  import raw from "../middleware/route.async.wrapper.mjs";
  import express from 'express';
  import log from '@ajar/marker';
  import {register_get, register_post, login_get, login_post, logout_post} from '../controllers/auth.controller.mjs'
  const router = express.Router();
  
  // parse json req.body on post routes
  router.use(express.json())
  

router.route('/register')
    .get(raw(register_get))
    .post(raw(register_post))

router.route('/login')
    .get(raw(login_get))
    .post(raw(login_post))

router.get("/logout", raw(logout_post));

  
  
  
 export default router;
  