
import bcrypt from 'bcryptjs';
import log from '@ajar/marker';
import { v4 as uuidv4 } from 'uuid';
import connection from '../db/sql.connection.mjs'
import {
    verify_token,
    false_response,
    tokenize
} from '../middleware/auth.middleware.mjs';
// module.exports.register = async (req, res) => {
//     try{
//         const {email, username, password} = req.body;
//         const user = new User({email, username});
//         const registeredUser = await User.register(user, password);
//         req.login(registeredUser, err => {
//             if(err) return next(err);
//             req.flash('success','Welcome to Camp site!');
//             res.redirect('/campgrounds');
//         });
//     } catch(e){
//         req.flash('error', e.message);
//         res.redirect('register');
//     } 
// }


export const register_get = async (req, res) => {
    res.send("register page...")
}

export const register_post = async (req, res) => {
    log.obj(req.body, 'register, req.body:')

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    log.info('hashedPassword:', hashedPassword)
    const user_data = {
        ...req.body,
        password: hashedPassword
    }
    // create a user
    const [rows] = await connection.query(`INSERT INTO draw.users (id, first_name, last_name, email, password) values ("${uuidv4()}" ,"${user_data.first_name}", "${user_data.last_name}","${user_data.email}", "${user_data.password}");`)
    // create a token
    const token = tokenize(rows.id)
    log.info('token:', token)

    return res.status(200).json({
        auth: true,
        token,
        user: rows
    })
}

export const login_get = async (req, res) => {
    res.send("login page...")
}


export const login_post = async (req, res) => {
    const { email, password } = req.body;
    console.log(email, password)
    //look for the user in db by email
    let [rows] = await connection.query(`SELECT * FROM users WHERE email="${email}"; `)
    //if no user found...
    if (rows.length === 0) return res.status(401).json({ ...false_response, message: "wrong email or password" })
    rows = rows[0]
    console.log(rows)
    // check if the password is valid
    const password_is_valid = await bcrypt.compare(password, rows.password)
    console.log(password_is_valid)
    if (!password_is_valid) return res.status(401).json({ ...false_response, message: "wrong email or password" })

    // if user is found and password is valid
    // create a fresh new token
    const token = tokenize(rows.id)
    // return the information including token as JSON
    return res.status(200).json({
        auth: true,
        token,
        user: { id: rows.id, first_name: rows.first_name, last_name: rows.last_name, email: rows.email }
    })
}
export const logout_post = async (req, res) => {
    const { email, username, password } = req.body;
    res.send("logout post");
} 