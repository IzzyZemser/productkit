export const validateFeature = (req, res, next) => {
    const body = req.body
    if(body.title && body.description && body.status >=0 && body.status <=2){
        next()
    }else{
        next("invalid feature");
    }
}