import connection from '../db/sql.connection.mjs'


export const show_all_users = async (req, res) => {
    const [rows] = await connection.query('SELECT * FROM draw.users;')
    res.status(200).json(rows);
}

export const add_user = async (req, res) => {
    log.obj(req.body, 'register, req.body:')

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    log.info('hashedPassword:', hashedPassword)
    const user_data = {
        ...req.body,
        password: hashedPassword
    }
    // create a user
    const [rows] = await connection.query(`INSERT INTO draw.users (id, first_name, last_name, email, password) values ("${uuidv4()}" ,"${user_data.first_name}", "${user_data.last_name}","${user_data.email}", "${user_data.password}");`)
    // create a token
    const token = tokenize(rows.id)
    log.info('token:', token)

    return res.status(200).json({
        auth: true,
        token,
        user: rows
    })
}

export const get_user = async (req, res) => {
    const id = req.params.id ?req.params.id : req.user_id;
    if(!id) {
        throw new Error("no id was given at get_user in user controller")
    }
    const [rows] = await connection.query(`SELECT * FROM playground.data WHERE id=${id}`)
    if (rows.length === 0) {
        return res.status(404).send("User not found");
    } else {
        res.status(200).json(rows);
    }
}

export const get_users_paginate = async(req, res)=> {
    let { page = 0 ,items = 10 } = req.params;
    const [rows] =  await connection.query(`SELECT * FROM data LIMIT ${parseInt(items)} OFFSET ${parseInt(page * items)}`)
    res.status(200).json(rows);
  };

  export const delete_user = async (req, res) => {
    const [rows] =  await connection.query(`DELETE FROM data WHERE id=${req.params.id};`)
    res.status(200).json(rows);
  
  }