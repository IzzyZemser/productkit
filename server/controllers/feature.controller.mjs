
import connection from '../db/sql.connection.mjs'
import { v4 as uuidv4 } from 'uuid';

export const get_all_features = async () => {
    const sql_query = 'SELECT * FROM draw.features;'
    const [rows] =  await connection.query(sql_query)
    res.status(200).json(rows);
}
export const get_features = (status) =>  async (req, res) => {
    const sql_query = `SELECT * FROM draw.features WHERE status=${status};`
    const [rows] =  await connection.query(sql_query)
    res.status(200).json(rows);
}
export const post_feature = async (req, res) => {
    const values = [uuidv4(), req.title, req.description, req.status, uuidv4(), uuidv4(), req.user_id]
    const sql_query = `INSERT INTO draw.features VALUES (${values});`
    const [rows] =  await connection.query(sql_query)
    res.status(200).json(rows);
}
