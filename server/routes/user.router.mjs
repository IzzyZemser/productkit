import raw from "../middleware/route.async.wrapper.mjs";
import express from 'express';
import {verify_token} from '../middleware/auth.middleware.mjs'

import { show_all_users,add_user, get_user, get_users_paginate, delete_user} from '../controllers/user.controller.mjs';
const router = express.Router();

// parse json req.body on post routes
router.use(express.json())



router.route('/')
  .get(raw(show_all_users))
  .post(raw(add_user))

// GET SINGLE USER
// router.get("/:id", raw(get_user));
// router.get(verify_token, "/user", raw(get_user));

//PAGINATE
router.get('/paginate/:page?/:items?', raw(get_users_paginate))

router.delete("/:id",raw(delete_user));


export default router;
