import raw from "../middleware/route.async.wrapper.mjs";
import express from 'express';
import log from '@ajar/marker';
import {get_all_features, get_features, post_feature} from '../controllers/feature.controller.mjs'
import {validateFeature} from '../data_validation/feature.validation.mjs'
import {verify_token} from '../middleware/auth.middleware.mjs'

const router = express.Router();

// parse json req.body on post routes
router.use(express.json())


router.route('/all')
  .get(raw(get_all_features))


router.route('/under-review')
  .get(raw(get_features(0)))
  .post(verify_token, validateFeature, raw(post_feature))

router.route('/planned')
  .get(validateFeature, raw(get_features(1)))
  .post(verify_token, validateFeature, raw(post_feature))

router.route('/in-progress')
  .get(raw(get_features(2)))
  .post(verify_token, validateFeature, raw(post_feature))





export default router;
