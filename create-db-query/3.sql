CREATE TABLE `users`(
    `id` BINARY(16) NOT NULL,
    `first_name` VARCHAR(255) NOT NULL,
    `last_name` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `profile_img` VARCHAR(255) NULL,
    `password` VARCHAR(255) NOT NULL,
    `access` INT NULL
);
ALTER TABLE
    `users` ADD PRIMARY KEY `users_id_primary`(`id`);
CREATE TABLE `features`(
    `id` BINARY(16) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `description` TEXT NOT NULL,
    `status` INT NOT NULL,
    `activity_id` INT NOT NULL,
    `like_id` BINARY(16) NOT NULL,
    `author_id` INT NOT NULL
);
ALTER TABLE
    `features` ADD PRIMARY KEY `features_id_primary`(`id`);
CREATE TABLE `activity`(
    `id` BINARY(16) NOT NULL,
    `user_id` BINARY(16) NOT NULL,
    `description` TEXT NOT NULL,
    `feature_id` INT NOT NULL
);
ALTER TABLE
    `activity` ADD PRIMARY KEY `activity_id_primary`(`id`);
CREATE TABLE `likeToUser`(
    `id` BINARY(16) NOT NULL,
    `user_id` INT NOT NULL,
    `like_id` BINARY(16) NOT NULL
);
ALTER TABLE
    `likeToUser` ADD PRIMARY KEY `liketouser_id_primary`(`id`);
ALTER TABLE
    `activity` ADD CONSTRAINT `activity_user_id_foreign` FOREIGN KEY(`user_id`) REFERENCES `users`(`id`);
ALTER TABLE
    `features` ADD CONSTRAINT `features_activity_id_foreign` FOREIGN KEY(`activity_id`) REFERENCES `activity`(`id`);
ALTER TABLE
    `features` ADD CONSTRAINT `features_author_id_foreign` FOREIGN KEY(`author_id`) REFERENCES `users`(`id`);
ALTER TABLE
    `likeToUser` ADD CONSTRAINT `liketouser_user_id_foreign` FOREIGN KEY(`user_id`) REFERENCES `users`(`id`);